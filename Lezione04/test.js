/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : test.js.js
 *******************************************/
/* stampo nome e cognome*/
var nome;
nome = "Alberto";

var cognome = "Amatruda";
var eta = 19;

console.log("Esercizio Javascript 1");
console.log(nome);
console.log(nome+cognome);
console.log(nome+" "+cognome+" "+eta);
console.log("tra 10 anni:"+(eta+10));
console.log("eta *2 ="+(eta*2));
console.log("eta /2 ="+(eta/2));
console.log("eta %2 ="+(eta%2));
console.log(eta);
console.log("eta ++ ="+(eta++));  /*Quando viene messo ++ o -- prima della variabile il valore aumenta subito, al contrario aumenta la riga successiva */
console.log(eta);
console.log("eta -- ="+(eta--));
console.log(eta);
console.log("eta -= ="+(eta-=10)); /*Per questo non vale il commento precedente */
console.log(eta);
console.log("-------------");
console.log("true="+(true));
console.log("!true="+(!true));
console.log("false="+(false));
console.log("!false="+(!false));
console.log("1==2:"+(1==2));
console.log("!1==2:"+(!(1==2)));
console.log("1=='2':"+(1!==2));
console.log("1==\"2\":"+(1=="1"));  /*Converte la stringa in numero e diventa uguale*/
console.log("1===\"2\":"+(1==="1"));  /*Con il triplo uguale trova la differneza tra stringa e numero con conseguente differenza*/


