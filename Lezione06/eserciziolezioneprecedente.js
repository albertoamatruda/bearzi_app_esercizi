function contalettere(frase) {
    //Trasformo la frase in minuscolo
    frase = frase.toLowerCase();
    //Trasformo la frase in un'array di caratteri singoli
    var arr_caratteri = frase.split("");
    //Creo un hash dove salvare il contatore di ogni lettera trovata
    var risultato = {};
    //definisco l'array dei caratteri ammessi
    var ammessi = ("abcdefghilmnopqrstuvzàèìòùxyw").split("");
    //Faccio un loop su tutte le lettere
    for(var i=0;i<arr_caratteri.length;i++){
        //salvo in una variabile temporanea la lettera corrente
        var lettera = arr_caratteri[i];
        if (ammessi.indexOf(lettera)>=0){
            //Aumento il contatore della lettera trovata
        if(risultato[lettera]){
            risultato[lettera] += 1;
        } else{
            risultato[lettera] = 1;
        }
        }
    }
    
    //Restituisco il risultato
    return risultato;
}
function stampaInConsole(oggetto){
    for(var i in oggetto){
        var contatore = oggetto[i];
        console.log("La lettera "+i+" compare "+contatore+" volte nella frase")
    }
}
//Definisco la frase da controllare
var frase = "Buongiorno, questa è la sesta lezione al bearzi";
//Richiamo la funzione
var risultato = contalettere(frase);
stampaInConsole(risultato);